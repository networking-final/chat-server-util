package com.gitlab.iron_e.networking_final.chat_server_util.commands;

import java.io.IOException;

import com.gitlab.iron_e.networking_final.chat_server_util.io.files.RoomDir;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used to configure {@code <room.json>} files.
 *
 * @author Iron_E
 * @since 2020-04-08
 */
public abstract class Configurer
{
	/**
	 * Configure some {@code <room.json>} at {@code path}.
	 * All of the parameters may be {@code null} except for {@code path}, but operations will only be performed for non-{@code null} parameters.
	 *
	 * @param path the filepath to the {@code <room.json>}.
	 * @param append a list of {@code <client_name>}s to append to the {@code <room.json>}.
	 * @param delete a list of {@code <client_name>}s to remove from the {@code <room.json>}.
	 * @param newName a new name for the {@code <room.json>}.
	 * @param priv whether or not the {@code <room.json>} is private.
	 */
	public static void config(String path, Boolean priv, String newName, String[] append, String[] delete)
	{
		try
		{
			final RoomDir dir = new RoomDir(path);
			final RoomDir.File TARGET_FILE = RoomDir.File.CONFIG;
			dir.write(Configurer.updateConfig(dir.read(TARGET_FILE), priv, newName, append, delete), TARGET_FILE);
		}
		catch (IOException | JSONException e)
		{
			System.out.println("There was an issue accessing the file.");
			e.printStackTrace();
		}
	}

	/**
	 * Update some {@code <room.json>} configuration with the provided parameters.
	 *
	 * @param config the {@link JSONObject} that represents the {@code <room.json>}.
	 * @param priv whether or not the room is publicly visible. <b>Nullable</b>.
	 * @param newName the new name for the room. <b>Nullable</b>.
	 * @param append an array of {@code <client_name>}s to append to the "{@code clients}" field. <b>Nullable</b>.
	 * @param delete an array of {@code <client_name>}s to delete from the "{@code clients}" field. <b>Nullable</b>.
	 * @return the {@code config} with the values of the non-{@code null} parameters.
	 */
	private static JSONObject updateConfig(
		final JSONObject config, final Boolean priv, final String newName, final String[] append, final String[] delete
	)
	{
		final String CLIENT_KEY = "clients";

		if (newName != null)
		{ config.put("name", newName); }

		if (priv != null)
		{ config.put("private", priv); }

		if (append != null || delete != null)
		{
			// Make sure that there is a list of clients to put the args into.
			if (!config.has(CLIENT_KEY))
			{ config.put(CLIENT_KEY, new JSONArray()); }

			// Put the array into a variable so there isn't a need to look it up in the table every time.
			final JSONArray clientArray = ((JSONArray) config.get(CLIENT_KEY));

			// Append to the list, given that there is a list to begin with.
			if (append != null)
			{
				for (int i = 0; i < append.length; ++i)
				{ clientArray.put(append[i]); }
			}

			// Delete from the list, given that there is a list to begin with.
			if (delete != null)
			{
				// Iterate over the client array.
				for (int i = 0; i < clientArray.length();)
				{
					// Iterate over the list of names to delete.
					for (final String clientName : delete)
					{
						// If the `clientArray[i]` is equal to the `clientName`, then remove it and continue with the next `clientArray[i]`.
						if (clientArray.get(i).equals(clientName))
						{ clientArray.remove(i); break; }
					}
					// Assuming the `continue outer;` line was not reached, increase the index so that the next word can be looked at.
					++i;
				}
			}
		}
		return config;
	}
}
