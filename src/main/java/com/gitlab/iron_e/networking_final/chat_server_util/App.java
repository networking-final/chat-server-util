package com.gitlab.iron_e.networking_final.chat_server_util;

import java.util.Arrays;

import com.gitlab.iron_e.networking_final.chat_server_util.commands.Cleaner;
import com.gitlab.iron_e.networking_final.chat_server_util.commands.Configurer;
import com.gitlab.iron_e.networking_final.chat_server_util.fmt.DateTime;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * The main class for the application.
 */
public class App
{
	/** Type alias for Exception that is thrown when the user needs help.  */
	private static class HelpException extends Exception
	{ private static final long serialVersionUID = 1L; }

	/** The command string that will execute the "clean" branch of the code. */
	private static final String CMD_CLEAN  = "clean";
	/** The command string that will execute the "config" branch of the code. */
	private static final String CMD_CONFIG = "config";
	/** Standard usage information about the commands {@link App#CMD_CLEAN} and {@link App#CMD_CONFIG}. */
	private static final String USAGE_COMMANDS  = "Please issue a command:" +
		"\n\tchat-server-util clean [options] <web_dir>" +
		"\n\tchat-server-util config [options] <room_dir>";

	/**
	 * @param args the arguments that the program will interpret.
	 */
	public static void main(String[] args)
	{
		try
		{
			// Create the options pool and add the "help" option as the only default.
			final Options options = new Options();
			final Option help = App.newOption("help", false, "Print this message.");
			options.addOption(help);

			// Split the command from the arguments.
			final String command = args[0];
			args = Arrays.copyOfRange(args, 1, args.length);

			switch (command)
			{
				case App.CMD_CLEAN:
					// Create the options.
					final Option expire = App.newOption(
						"expire", true, "Set the oldest time that a message should be allowed to stay alive on the server during cleaning."
					);
					final Option clients = App.newOption(
						"clients", false, "Remove clients who have not logged in recently. Requires `--expire`."
					);

					// Set the number of arguments accepted by each option.
					expire.setArgs(2);

					// Set arg names.
					expire.setArgName("yyyy-mm-dd [hh-mm-ss]");

					// Add the options to the list of options that are parsed.
					options.addOption(clients);
					options.addOption(expire);

					try
					{
						// Parse the options.
						final CommandLine cleanCmd = new DefaultParser().parse(options, args);

						// Make sure the user doesn't need help before launching into an operation.
						if (cleanCmd.hasOption(help.getOpt()))
						{ throw new App.HelpException(); }
						else
						{
							final String[] expiry = cleanCmd.getOptionValues(expire.getOpt());

							// Format the expire field.
							int[][] expiryFmtd = null;
							if (expiry != null)
							{ expiryFmtd = DateTime.format(expiry); }

							// Run the cleaner.
							Cleaner.clean(
								cleanCmd.getArgList().get(0),
								expiryFmtd,
								cleanCmd.hasOption(clients.getOpt())
							);
						}
					}
					catch(App.HelpException | IndexOutOfBoundsException ignored)
					{ new HelpFormatter().printHelp(App.USAGE_COMMANDS.split("\n\t")[1], options); }
					break;

				case App.CMD_CONFIG:
					// Initialize common usage strings.
					final String APPEND_DELETE_USAGE = "A comma separated list of <client_name>s to ";
					final String PRIV_USAGE = "Set the room's visibility to ";
					final String INCOMPAT = " Incompatable with -";

					// Create the options.
					final Option append = App.newOption("append", true, APPEND_DELETE_USAGE + "append.");
					final Option delete = App.newOption("delete", true, APPEND_DELETE_USAGE + "remove.");
					final Option name   = App.newOption(
						"name", true, "Adjust the name of the room to the name set by this option."
					);
					final Option priv   = App.newOption("private", false, PRIV_USAGE+"private.");
					final Option noPriv = new Option(
						"np", "no-"+priv.getLongOpt(), false, PRIV_USAGE+"public."+INCOMPAT+priv.getOpt()+"."
					);

					// Update descriptions.
					priv.setDescription( priv.getDescription()+INCOMPAT+noPriv.getOpt()+"." );

					// Set the number of arguments to be selected by each option.
					append.setArgs(Option.UNLIMITED_VALUES);
					delete.setArgs(Option.UNLIMITED_VALUES);
					name.setArgs(1);

					// Set arg names.
					final String ARG_CLIENT = "client name";
					append.setArgName(ARG_CLIENT);
					delete.setArgName(ARG_CLIENT);
					name.setArgName("room name");

					// Set value separators for the options that take more than one argument.
					append.setValueSeparator(',');
					delete.setValueSeparator(',');

					// Create groups of mutually exclusive options.
					final OptionGroup privGroup = new OptionGroup();
					privGroup.addOption(noPriv);
					privGroup.addOption(priv);

					// Add the newly created options to the list of options that are parsed.
					options.addOption(append);
					options.addOption(delete);
					options.addOption(name);
					options.addOptionGroup(privGroup);

					try
					{
						// Parse the options
						final CommandLine configCmd = new DefaultParser().parse(options, args);

						// Make sure the user doesn't need help before launching into an operation.
						if (configCmd.hasOption(help.getOpt()))
						{ throw new App.HelpException(); }
						else
						{
							// Determine whether or not a privacy setting was specified — and if so — what the value is.
							Boolean newPriv = null;
							if ( configCmd.hasOption(priv.getOpt()) )
							{ newPriv = true; }
							else if ( configCmd.hasOption(noPriv.getOpt()) )
							{ newPriv = false; }

							// Run the config util.
							Configurer.config(
								configCmd.getArgList().get(0), // the path to the file
								newPriv, // the new privacy setting
								configCmd.getOptionValue(name.getOpt()), // the name option
								configCmd.getOptionValues(append.getOpt()), // the append option
								configCmd.getOptionValues(delete.getOpt()) // the delete option
							);
						}
					}
					catch (App.HelpException | IndexOutOfBoundsException ignored)
					{ new HelpFormatter().printHelp(App.USAGE_COMMANDS.split("\n\t")[2], options); }
					break;

				default:
					System.out.println(App.USAGE_COMMANDS);
			}
		}
		catch (ArrayIndexOutOfBoundsException ignored)
		{ System.out.println(App.USAGE_COMMANDS); }
		catch (NumberFormatException | ParseException e)
		{
			System.out.println("There was an error parsing the args.");
			e.printStackTrace();
		}
	}

	/**
	 * Create an {@link org.apache.commons.cli.Option Option} without having to specify the {@link org.apache.commons.cli.Option#getOpt short name}.
	 *
	 * @param name the name of the {@linkplain org.apache.commons.cli.Option Option}.
	 * @param hasArgs whether or not the {@linkplain org.apache.commons.cli.Option Option} takes arguments.
	 * @param description the description of the {@linkplain org.apache.commons.cli.Option Option}.
	 * @return the new {@linkplain org.apache.commons.cli.Option Option}.
	 */
	private static Option newOption(final String name, final boolean hasArgs, final String description)
	{ return new Option(name.substring(0, 1), name, hasArgs, description); }
}
