package com.gitlab.iron_e.networking_final.chat_server_util.io.files;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Used to read from and write to files in a directory.
 *
 * @author Iron_E
 * @since 2020-04-09
 */
abstract class Dir
{
	/** The path to the directory that this class represents. */
	protected final String path;

	/**
	 * Create a new {@code Dir} that performs operations on the specified {@code path}.
	 *
	 * @param path the path to the {@code <room_dir>}.
	 */
	protected Dir(final String path) { this.path = path; }

	/**
	 * @return the {@link Dir#path}.
	 */
	public String getPath() { return path; }

	/**
	 * Get the {@linkplain JSONObject JSON} from some {@code file}.
	 *
	 * @return the {@link JSONObject} created by reading {@code file}.
	 * @throws IOException if there is an issue reading the {@code file} from {@linkplain Dir#path path}.
	 * @throws JSONException if there is an issue unmarshalling the JSON inside of the file at {@code path}.
	 */
	protected <T> T read(final String filepath, final Class<T> type) throws IOException, JSONException
	{
		try ( final InputStream is = Files.newInputStream( Path.of(this.path, filepath) ); )
		{ return type.cast(new JSONTokener(is).nextValue()); }
		catch (IOException | JSONException  e)
		{ throw e; }
	}

	/**
	 * Write some provided {@linkplain JSONObject JSON} to a target {@code file}.
	 *
	 * @param json the json to write to the file.
	 * @param path the file to write to.
	 * @throws IOException if there is an issue writing to the file.
	 */
	protected void write(final JSONObject json, final String filepath) throws IOException
	{
		try
		( // †
			final BufferedWriter bw = Files.newBufferedWriter(
				Path.of(this.path, filepath), StandardOpenOption.TRUNCATE_EXISTING
			);
		) // ‡
		{ json.write(bw); }
		catch ( IOException e )
		{ throw e; }
	}
}

