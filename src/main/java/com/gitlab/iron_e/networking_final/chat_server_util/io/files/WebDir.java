package com.gitlab.iron_e.networking_final.chat_server_util.io.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used to read from and write to a {@code <web_dir>}.
 *
 * @author Iron_E
 * @since 2020-04-09
 */
public class WebDir extends Dir
{
	/**
	 * List of files and paths that are compatable with the {@code Iron_E/chat-server} specification.
	 *
	 * @author Iron_E
	 * @since 2020-04-09
	 */
	public static enum File
	{ // †
		CLIENTS("clients.json"),
		QUEUE("queue.json");

		private final String path;
		private File(final String path) { this.path = path; }
	} // ‡

	/**
	 * Create a new {@code WebDir} that performs operations on the specified {@code path}.
	 *
	 * @param path the path to the {@code <room_dir>}.
	 */
	public WebDir(final String path) { super(path); }

	/**
	 * Get the {@linkplain JSONObject JSON} from some {@code file}.
	 *
	 * @return the {@link JSONObject} created by reading {@code file}.
	 * @throws IOException if there is an issue reading the {@code file} from {@linkplain WebDir#path path}.
	 * @throws JSONException if there is an issue unmarshalling the JSON inside of the file at {@code path}.
	 */
	public JSONObject read(final WebDir.File file) throws IOException, JSONException { return super.read(file.path, JSONObject.class); }

	/**
	 * Read the "rooms" directory of a {@link WebDir#path}.
	 * 
	 * @return a stream of {@link RoomDir}s that are present in {@link WebDir#path}.
	 * @throws IOException
	 */
	public Stream<RoomDir> readRooms() throws IOException
	{ return Files.list(Path.of(path, "rooms")).filter(Files::isDirectory).map(RoomDir::new); }

	/**
	 * Write some provided {@linkplain JSONObject JSON} to a target {@code file}.
	 *
	 * @param json the json to write to the file.
	 * @param file the file to write to.
	 * @throws IOException if there is an issue writing to the file.
	 */
	public void write(final JSONObject json, final WebDir.File file) throws IOException { super.write(json, file.path); }
}
