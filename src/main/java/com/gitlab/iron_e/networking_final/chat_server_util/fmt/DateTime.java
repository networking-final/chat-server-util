package com.gitlab.iron_e.networking_final.chat_server_util.fmt;

/**
 * Utility class that handles converting to and from proper time encoding in the {@code Iron_E/chat-server} spec.
 *
 * @author Iron_E
 * @since 2020-04-08
 */
public abstract class DateTime
{
	/** The length of the inside of the array that is <b>returned</b> by {@link DateTime#format(String[])} and <b>accepted</b> by {@link DateTime#compare(int[][], int[][])}. */
	public static final int ARRAY_LENGTH_INNER = 2;
	public static final int ARRAY_LENGTH_OUTER = 3;

	/**
	 * Format a date-time string in "yyyy-mm-dd hh-mm-ss" format and turn it into an <code>int[]{ [yyyy,mm,dd],[hh,mm,ss] }</code>
	 *
	 * @param dateTime the unformatted date-time {@link String}.
	 * @return the formatted {@code dateTime}.
	 */
	public static int[][] format(String[] dateTime)
	{
		final int[][] dateTimeFmtd = new int[2][3];
		for (int i = 0; i < dateTime.length; ++i)
		{
			final String[] split = dateTime[i].split("-");
			for (int j = 0; j < split.length; ++j)
			{
				dateTimeFmtd[i][j] = Integer.parseInt(split[j]);
			}
		}
		return dateTimeFmtd;
	}

	/**
	 * Compares two date-times and returns the relation of {@code dateTime1} to {@code dateTime2}.
	 *
	 * @param dateTime1 the first {@code int[][]} to compare.
	 * @param dateTime2 the second {@code int[][]} to compare.
	 * @return {@code -1} if {@code dateTime1} is older than {@code dateTime2}, {@code 0} if they are the same, and {@code 1} if it is more recent.
	 * @throws IllegalArgumentException if {@code dateTime1} and {@code dateTime2} aren't arrays of the same size.
	 *
	 * @see DateTime#format(String[])
	 */
	public static int compare(int[][] dateTime1, int[][] dateTime2) throws IllegalArgumentException
	{
		try
		{
			for (int i = 0; i < DateTime.ARRAY_LENGTH_OUTER; ++i)
			{
				for (int j = 0; j < DateTime.ARRAY_LENGTH_INNER; ++j)
				{
					// One of the fields in the first date-time is less than that of the second— therefore the rest must be also.
					if (dateTime1[i][j] < dateTime2[i][j])
					{ return -1; }
					// One of the fields in the first date-time is greater than that of the second— therefore the rest must be also.
					else if (dateTime1[i][j] > dateTime2[i][j])
					{ return 1; }
				}
			}
			// None of the dates or times was greater or less than each other, so they must be the same.
			return 0;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{ throw new IllegalArgumentException("The `int[][]`s were not formatted correctly.", e); }
	}
}

