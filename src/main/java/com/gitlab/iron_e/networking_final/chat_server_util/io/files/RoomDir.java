package com.gitlab.iron_e.networking_final.chat_server_util.io.files;

import java.io.IOException;
import java.nio.file.Path;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used to read from and write to a {@code <room_dir>}.
 *
 * @author Iron_E
 * @since 2020-04-09
 */
public class RoomDir extends Dir
{
	/**
	 * List of files and paths that are compatable with the {@code Iron_E/chat-server} specification.
	 *
	 * @author Iron_E
	 * @since 2020-04-09
	 */
	public static enum File
	{ // †
		CONFIG("room.json"),
		MESSAGE_CACHE("message_cache.json");

		private final String path;
		private File(final String path) { this.path = path; }
	} // ‡

	/** The id of the {@linkplain RoomDir room}. Derived from {@link RoomDir#path}. */
	public final String id;

	/**
	 * Create a new {@code RoomDir} that performs operations on the specified {@code path}.
	 *
	 * @param path the path to the {@code <room_dir>}.
	 */
	public RoomDir(final String path)
	{
		super(path);
		final String[] splitPath = path.split(java.io.File.separator);
		this.id = splitPath[splitPath.length-1];
	}

	/**
	 * Create a new {@code RoomDir} that performs operations on the specified {@code path}.
	 *
	 * @param path the path to the {@code <room_dir>}.
	 */
	protected RoomDir(final Path path) { this(path.toString()); }

	/**
	 * Get the {@linkplain JSONObject JSON} from some {@code file}.
	 *
	 * @return the {@link JSONObject} created by reading {@code file}.
	 * @throws IOException if there is an issue reading the {@code file} from {@linkplain RoomDir#path path}.
	 * @throws JSONException if there is an issue unmarshalling the JSON inside of the file at {@code path}.
	 */
	public JSONObject read(RoomDir.File file) throws IOException, JSONException { return super.read(file.path, JSONObject.class); }

	/**
	 * Write some provided {@linkplain JSONObject JSON} to a target {@code file}.
	 *
	 * @param json the json to write to the file.
	 * @param file the file to write to.
	 * @throws IOException if there is an issue writing to the file.
	 */
	public void write(final JSONObject json, final RoomDir.File file) throws IOException { super.write(json, file.path); }
}
