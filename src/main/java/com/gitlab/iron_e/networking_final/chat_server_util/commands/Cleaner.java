package com.gitlab.iron_e.networking_final.chat_server_util.commands;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gitlab.iron_e.networking_final.chat_server_util.fmt.DateTime;
import com.gitlab.iron_e.networking_final.chat_server_util.io.files.RoomDir;
import com.gitlab.iron_e.networking_final.chat_server_util.io.files.WebDir;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Performs cleaning operations on {@code <web_dir>}s.
 *
 * @author Iron_E
 * @since 2020-04-08
 */
public abstract class Cleaner
{
	/**
	 * Clean the files inside of {@code path} of unecessary or old data.
	 *
	 * @param path the path to a {@code <web_dir>} that's cleaning is desired.
	 * @param expire the  oldest {@link DateTime date} of origin that a message will be allowed to still exist with. <b>Nullable</b>.
	 */
	public static void clean(final String path, final int[][] expire, final boolean expireClients)
	{
		// NOTE: see documentation for all of the `Cleaner` methods being called here. They cover the operations being performed on an in-depth basis and do so in a way that doesn't clutter the entire screen at once.
		try
		{
			if (expire != null)
			{
				final HashSet<String> oldClients = Cleaner.expireMessageRefs(path, Cleaner.expireMessages(path, expire), expireClients);

				if (expireClients)
				{ Cleaner.expireClients(path, oldClients); }
			}
			Cleaner.removeBrokenLinks(path);
		}
		catch (IOException | NullPointerException e)
		{ System.out.println("There was a problem cleaning the <web_dir>."); e.printStackTrace(); }
	}

	// ========================================================================= //

	/**
	 * Format {@code rawTime} in a way that {@link DateTime} can handle.
	 * @param rawTime the raw string literal of time for a {@code <message>}.
	 * @return the formatted {@link String}.
	 */
	private static String[] dateTime(String rawTime)
	{
		// Split across underscore.
		final String[] time = rawTime.split("_");
		// Iterate over split and replace dashes with spaces.
		for (int i = 0; i < time.length; ++i)
		{ time[i] = String.join(" ", time[i].split("-")); }
		// Return the result.
		return time;
	}

	/**
	 * Expire clients who have not been active to receive a message that was old enough to be removed by the clean operation.
	 *
	 * @param path the path to the {@code <room_dir>} where the {@code <clients.json>} is.
	 * @param oldClients the set of old client names.
	 * @throws IOException if there is a problem performing IO operations on {@code path}.
	 * @throws JSONException if there is an issue unmarshalling the {@code <clients.json>}.
	 */
	private static void expireClients(final String path, final Set<String> oldClients) throws IOException, JSONException
	{
		// Variable for <web_dir> at `path`.
		final WebDir webDir = new WebDir(path);
		// The file we want to read.
		final WebDir.File TARGET_FILE = WebDir.File.CLIENTS;
		// Read the JSON inside `TARGET_FILE`.
		final JSONObject clientsDotJson = webDir.read(TARGET_FILE);

		// Iterate over the <client_name>s inside of `clientsDotJson`.
		for ( final Iterator<String> clientNames = clientsDotJson.keys(); clientNames.hasNext(); )
		{
			// Get the next clientName.
			final String clientName = clientNames.next();

			// If the client is old, remove it from the JSON object.
			if ( oldClients.contains(clientName) )
			{ clientsDotJson.remove(clientName); }
		}

		// Write the JSON back to `TARGET_FILE`.
		webDir.write(clientsDotJson, TARGET_FILE);
	}

	/**
	 * Remove all references in a {@code <queue.json>} to messages that were {@link Cleaner#expireMessages(String, int[][]) removed}.
	 *
	 * @param path the path to the {@code <web_dir>} containing the {@code <queue.json}.
	 * @param expiredMessages the {@link Iterable} containing expired messages mapped to their room ids.
	 * @param expireClients whether or not to remove old clients, instead of their old messages.
	 * @returns a list of clients who had messages removed from their queue.
	 * @throws IOException if there is an issue editing the {@code <queue.json>} inside of {@code path}.
	 * @throws JSONException if the {@code <queue.json>} does not match the server specification.
	 */
	private static <V extends List<String>> HashSet<String> expireMessageRefs(
		final String path, final Map<String, V> expiredMessages, final boolean expireClients
	) throws IOException, JSONException
	{
		// Variable to hold the return value.
		final HashSet<String> oldClients = new HashSet<>();

		// Variable for the <web_dir> at `path`.
		final WebDir webDir = new WebDir(path);

		// The file to read.
		final WebDir.File TARGET_FILE = WebDir.File.QUEUE;

		// Read JSON from file.
		final JSONObject queueDotJson = webDir.read(TARGET_FILE);

		// Iterate over the names of clients that have queued messages.
		clientNames:for ( final String clientName : queueDotJson.keySet() )
		{ // †
			// Get the rooms that have expired messages.
			final JSONObject roomQueues = queueDotJson.getJSONObject(clientName);
			// Iterate over the rooms that have expired messages.
			for ( final String roomId : expiredMessages.keySet() )
			{ // †
				// If the client has queued messages for the room.
				if ( roomQueues.has(roomId) )
				{ // †
					// Iterate over the expired messages in the given room.
					for ( final String expiredId : expiredMessages.get(roomId) )
					{ // †
						// Get the queued messages for `clientName` in `roomId`.
						final JSONArray queuedMsgs = roomQueues.getJSONArray(roomId);
						// Iterate over the length of the messages that have been queued for the client.
						for (int i = 0; i < queuedMsgs.length(); ++i)
						{ // †
							// If any given message matches the expired message.
							if (queuedMsgs.getString(i) == expiredId)
							{
								if (expireClients)
								{
									// Remove the client from the queue.
									queueDotJson.remove(clientName);
									// Add the client to the list of old clients.
									oldClients.add(clientName);
									// Continue from the top for.
									continue clientNames;
								}
								else
								{
									// Remove it from the array.
									queuedMsgs.remove(i);
									// break from looping over the queued messages.
									break;
								}
							}
						} // ‡
					} // ‡
				} // ‡
			} // ‡
		} // ‡

		// Write JSON to file.
		webDir.write(queueDotJson, TARGET_FILE);

		// Return the list of old clients.
		return oldClients;
	}

	/**
	 * Look through all the messages in every {@code <message_cache.json>} in every {@code <room_dir>} in {@code path} and remove the ones that were sent on a date older than {@code expire}.
	 *
	 * @param path the path to the {@code <web_dir>}.
	 * @param expire the oldest {@link DateTime date} of origin that a message will be allowed to still exist with.
	 * @returns the {@code <message_id>}s of all removed {@code <message>}s mapped to the room that they came from.
	 * @throws IOException if there is an issue performing operations in {@code path}.
	 * @throws NullPointerException if there are required values missing from the {@code <web_dir>} structure or a {@code <message_cache.json>}.
	 */
	private static HashMap<String, LinkedList<String>> expireMessages(
		final String path, final int[][] expire
	) throws IOException, JSONException, NullPointerException
	{
		final HashMap<String, LinkedList<String>> removedMessages = new HashMap<>();

		// Iterate over the <room_dir>s in the <web_dir> `path`.
		for ( final Iterator<RoomDir> roomDirs = new WebDir(path).readRooms().iterator(); roomDirs.hasNext(); )
		{
			// Create a directory object.
			final RoomDir roomDir = roomDirs.next();
			final RoomDir.File TARGET_FILE = RoomDir.File.MESSAGE_CACHE;

			// Get the JSON from the target file.
			final JSONObject messageCacheDotJson = roomDir.read(TARGET_FILE);

			// Iterate over the message id keys.
			for ( final Iterator<String> msgIds = messageCacheDotJson.keys(); msgIds.hasNext(); )
			{ // †
				final String msgId = msgIds.next();

				// Grab the time frmo the `JSONObject`.
				final String[] time = Cleaner.dateTime(
					((JSONObject) messageCacheDotJson.get(msgId)).getString("time")
				);

				// if the newly formatted `time` is older than `expire`.
				if (DateTime.compare( DateTime.format(time),expire ) < 0)
				{
					// Remove it from the `JSONObject` and add it to the list of removed message ids.
					messageCacheDotJson.remove(msgId);

					// Grab the room id.
					final String roomId = roomDir.getPath();

					// Make sure that the map of removed messages has an entry for this room.
					if (!removedMessages.containsKey(roomId))
					{ removedMessages.put(roomId, new LinkedList<>()); }

					// Add this message.
					removedMessages.get(roomId).add(msgId);
				}
			} // ‡

			// Write changes to the disk.
			roomDir.write(messageCacheDotJson, TARGET_FILE);
		}
		return removedMessages;
	}

	/**
	 * Find and remove references to messages in {@code <message_cache.json>}s that don't exist in the {@code <queue.json>}.
	 *
	 * @param path the path to the {@code <web_dir>}.
	 * @throws IOException if there is an error reading or writing to a file.
	 */
	private static void removeBrokenLinks(final String path) throws IOException
	{
		// Variable to hold easily-looked-up values of rooms and their respective messages.
		final HashMap<String, Set<String>> roomIdsToQueuedMsgIds = new HashMap<>();

		// Get the web dir.
		final WebDir webDir = new WebDir(path);

		// Add scope.
		{ final JSONObject queueDotJson = webDir.read(WebDir.File.QUEUE); // Get the `queue.json`.
			// Iterate over all the clients in `queueDotJson`.
			for ( final Iterator<String> clients = queueDotJson.keys(); clients.hasNext(); )
			{
				// Get the next client name.
				final String clientName = clients.next();
				// Get all the rooms that have queued messages for that client.
				final JSONArray rooms = queueDotJson.getJSONArray(clientName);
				
				// Iterate over the IDs of rooms.
				for (int i = 0; i < rooms.length(); ++i)
				{
					// Variable to store roomId of current directory.
					final String roomId = rooms.getString(i);

					// If the directory hasn't been initialized, initialize it.
					if ( !roomIdsToQueuedMsgIds.containsKey(roomId) )
					{ roomIdsToQueuedMsgIds.put(roomId, new HashSet<>()); }

					// Iterate over all the queued messages and populate the list.
					for ( final Iterator<?> msgIds = rooms.iterator(); msgIds.hasNext(); )
					{ roomIdsToQueuedMsgIds.get(roomId).add( msgIds.next().toString() ); }
				}
			}
		}

		// Add scope.
		{ final RoomDir.File TARGET_FILE = RoomDir.File.MESSAGE_CACHE; // set the file to read and write from.
			// Iterate over the room dirs in the `web dir`.
			for ( final Iterator<RoomDir> roomDirs = webDir.readRooms().iterator(); roomDirs.hasNext(); )
			{
				// Get the room dir.
				final RoomDir roomDir = roomDirs.next();

				// Get the message cache in the room dir.
				final JSONObject messageCacheDotJson = roomDir.read(TARGET_FILE);

				final Set<String> queuedMsgIds = roomIdsToQueuedMsgIds.get(roomDir.id);

				// Iterate over all the <message_id>s in the <message_cache.json>.
				for ( final Iterator<String> msgIds = messageCacheDotJson.keys(); msgIds.hasNext(); )
				{
					// Get the next <message_id>.
					final String msgId = msgIds.next();

					// Remove `msgId` from the <message_cache.json> if it is not in the <queue.json>.
					if ( !queuedMsgIds.contains(msgId) )
					{ messageCacheDotJson.remove(msgId); }
				}

				// Write the changes to the disk.
				roomDir.write(messageCacheDotJson, TARGET_FILE);
			}
		}
	}
}
