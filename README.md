# Chat-Server-Util

## About

This section is about `chat-server-util`, an optional maintenance utility that helps users manage their `chat-server`s. It's goals are to help provide several functions that are not included in the main program, for the sake of breaking larger applications up into smaller ones when the focus of the work being done changes.

This application seeks to provide the following features:

* Clean `<clients.json>` files of inactive users.
	* This will allow __users__ to remove people from the server under particular time-related criteria.
	* Autoclean `<queue.json>` after to remove references to users that don't exist.
* Scan `<message_cache.json>` for `<message>` that have no references in a `<queue>`.
	* This will allow the __users__ to remove `<message>`s from the cache, thus reclaiming space on the hard drive.
* Change the settings of a `<room.json>` file.
	* This will allow __users__ to concretely display and alter settings of a certain room while maintaining the de-facto syntax of the specification.

This application does _not_ seek to provide the following features:

* Create `<room_dir>`s.
	* This is possible with a utility that comes with the `chat-server`. This was too essential to be stripped out of the main application.
* Generate traffic, performance, and/or usage statistics.
	* There are other tools available that can provide this functionality better (see `htop`, `powertop`, and `wireshark` as recommendations).
* View `<message>`s and/or alter their contents.
	* It is my intent to leave messages on a host machine for as little time as possible. Making them easily viewable and indexable not the intention of this project.

### Build Instructions

1. Run `mvn package` (or `mvn clean package` to build from scratch) in the root of the project directory.
	* This will produce a dependency-independent ".jar" file in the [target directory](./target).

### Dependendcies

* [Apache Commons CLI](https://mvnrepository.com/artifact/commons-cli/commons-cli)
* [Maven](https://maven.apache.org/)
* [OpenJDK 13+](https://jdk.java.net/13/) (or any JDK release 13+)
* [stleary/JSON-java](https://github.com/stleary/JSON-java)

## Documentation

Here `<CLEAN>` is defined as an operation to be performed on some file `$f`. This operation should _remove_ dubious or unnecessary data and ensure that other files that reference to `$f` are also `<CLEAN>`ed to reflect the new state of `$f`.

Here `<CONF>` is defined as an operation to be performed on some file `$f`. This operation should _alter_ data that is correctly formatted in regards to `$f`'s existing documentation. Any `$f` altered by `<CONF>` that was previously valid will remain valid after the operation.

### \<CLEAN> \<clients.json> →

```pseudocode
// Implemented by a <clients.json> object.
FUNCTION Clean RETURN nothing:
	DEF PARAM (mC)          AS (<message_cache.json>).
	DEF PARAM (oldest_date) AS (DateTime).
	DEF PARAM (qL)          AS (List OF <queue.json>).
	DEF THIS                AS (<clients.json>).

	FOREACH (<client_name>) IN THIS AS (name) LABEL ("outer"):
	{
		FOREACH (<room_id>) IN KEYS (qL[name]) AS (rId):
		{
			FOREACH (<message_id>) IN (qL[name][rId]) AS (mId):
			{
				IF DATE (mC[mId]["time"]) OLDER THAN (oldest_date):
				{
					DEL KEY (name) FROM (THIS).
					CONTINUE ("outer").
				}
			}
		}
	}

	FOREACH (<queue>) IN (qL) AS q:
	{
		CALL (q's Clean).
	}

	CALL (mC's Clean).
END
```


### \<CLEAN> \<message_cache.json> →

```pseudocode
// Implemented by a <message_cache.json> object.
FUNCTION Clean RETURN nothing:
	DEF PARAM (qL)  AS (List OF <queue.json>).
	DEF THIS        AS (<message_cache.json>).
	DEF VAR   (idL) AS (List OF UNIQUE <message_id>).

	FOREACH (<queue>) IN (qL) AS (q):
	{
		FOREACH (<client_name>) IN KEYS (q) AS (name):
		{
			FOREACH (<message_id>) IN (q[name]) AS (id):
			{
				APPEND (id) TO (idL).
			}
		}
	}

	FOREACH (<message_id>) IN KEYS (THIS) AS (id):
	{
		IF (id) NOT IN (idL).
		{
			DEL KEY (id) FROM (THIS).
		}
	}
END
```

### \<CLEAN> \<queue.json> →

```pseudocode
// Implemented by a <queue.json> object.
FUNCTION Clean RETURN nothing:
	DEF PARAM (qL) AS (List OF <queue.json>).
	DEF THIS       AS (<clients.json>).

	FOREACH (<client_name>) IN (qL) AS (name):
	{
		IF (name) NOT IN KEYS (THIS):
		{
			DEL KEY (name) FROM (qL).
		}
	}
END
```

### \<CONF> \<room.json>

```pseudocode
// Implemented by a <room.json> object.
FUNCTION Conf RETURN nothing:
	DEF THIS AS (<room.json>).

	DEF SHOW (config) AS PROMPT:
	{
		DEF OPTION (clients) AS PROMPT:
		{
			DEF OPTION (add) AS PROMPT:
			{
				DEF INPUT (clientName) AS FUNCTION:
				{
					IF (clientName) NOT IN (THIS´s clients):
					{
						APPEND (clientName) TO (THIS´s clients).
					}
				}
			}

			DEF OPTION (remove) AS PROMPT:
			{
				DEF INPUT (clientName) AS FUNCTION:
				{
					DEL (clientName) FROM (THIS´s clients).
				}
			}
		}

		DEF OPTION (name) AS PROMPT:
		{
			DEF INPUT (newName) AS FUNCTION:
			{
				SET (THIS name) TO (newName).
			}
		}

		DEF OPTION (private) AS PROMPT:
		{
			DEF OPTION (toggle) AS FUNCTION:
			{
				SET (THIS´s private) TO NOT (THIS´s private).
			}
		}

		DEF OPTION (quit) AS FUNCTION:
		{
			EXIT (config).
		}
	}

	SHOW (main).
END
```

## Usage

1. Run `java -jar target/chat_server_util-$VERSION.jar` after building the project.
	* "`$VERSION`" is the current version of the project. So, if the current version is `1.0.0`, then substitute that for `$VERSION`.
	* Do not run the "original-….jar"— that one does not have built-in dependencies.

> ### Note
>
> You can run `-h` to view the usage instructions of the utility.
